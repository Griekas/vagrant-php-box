Vagrant.require_version '>= 1.8.6'

required_plugins = %w(vagrant-hostsupdater)
plugins_to_install = required_plugins.select { |plugin| not Vagrant.has_plugin? plugin }

if plugins_to_install.any?
  puts "Installing plugins: #{plugins_to_install.join(' ')}"
  if system "vagrant plugin install #{plugins_to_install.join(' ')}"
    exec "vagrant #{ARGV.join(' ')}"
  else
    abort 'Installation of one or more plugins has failed. Aborting.'
  end
end

Vagrant.configure(2) do |config|
  config.vm.box = 'ubuntu/trusty64'
  config.vm.network :private_network, ip: '33.33.33.10'
  config.vm.hostname = 'site.dev'

  config.vm.synced_folder "site/", "/var/www/html", owner: "www-data", group: "www-data"

  forward_port = ->(guest, host = guest) do
    config.vm.network :forwarded_port,
      guest: guest,
      host: host,
      auto_correct: true
  end

  forward_port[1080]      # mailcatcher
  forward_port[3306]      # mysql
  forward_port[80, 8080]  # nginx/apache

  config.vm.provision :puppet do |puppet|
    puppet.manifests_path = 'manifests'
    puppet.manifest_file = 'default.pp'
  end
end
