# Vagrant PHP Box #

Create symlink to your php project:
```
ln -s path/to/your/php/project site
```

Start vagrant
```
vagrant up
```

Access your php site on: http://site.dev

### PhpMyAdmin
Available on http://side.dev/phpmyadmin
User: root Password: root
